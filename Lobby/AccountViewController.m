//
//  AccountViewController.m
//  Lobby
//
//  Created by Ivan Wan on 23/1/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import "AccountViewController.h"
#import "DEMORootViewController.h"

@interface AccountViewController ()

@property (nonatomic, weak) UITextField *activeTextField;

@end

@implementation AccountViewController
{
    NSString* name;
}

- (IBAction)logOut:(id)sender {
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    [prefs setObject:nil forKey:@"phone"];
    [prefs setObject:nil forKey:@"toKen"];
    [prefs setObject:nil forKey:@"userId"];
    [prefs setObject:nil forKey:@"nickname"];
    [prefs setObject:nil forKey:@"email"];
    [prefs setObject:nil forKey:@"customId"];
    [prefs setObject:nil forKey:@"sex"];
    
    UserSession* user = [UserSession sharedInstance];
    user.userId = nil;
    user.phone = nil;
    user.nickname = nil;
    user.email = nil;
    user.toKen = nil;
    user.customId = nil;
    user.sex = nil;

    user.digits_authToken = nil;
    user.digits_userID = nil;
    user.digits_authTokenSecret = nil;
    user.digits_phoneNumber = nil;
    user.point = 0;
    user.getPointSuccess = NO;
    
    if ([Digits sharedInstance] != nil)
        [[Digits sharedInstance] logOut];
    
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    if (loginManager != nil)
        [loginManager logOut];
    
    AppDelegate* appDelegate = LobbyAppDelegate;
    [appDelegate.demoRootViewController unwind];
}

- (void)sideMenuBtnAction {

    // only update when name changed
    if ([UserSession sharedInstance] && ![name isEqualToString:self.nameTextField.text]) {
        
        [[NSUserDefaults standardUserDefaults] setObject:self.nameTextField.text forKey:@"nickname"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UserSession* user = [UserSession sharedInstance];
        user.nickname = self.nameTextField.text;
        
        //Digits* digits = [Digits sharedInstance];
        
        NSString* nick = self.nameTextField.text;
        NSString* phone = user.phone;
        NSString* token = user.toKen;
        NSString* sign = @"";
        NSString* param = [NSString stringWithFormat:@"nick=%@&phone=%@&token=%@&sign=%@",nick,phone,token,sign];
        NSString* param2 = [param stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        param2 = [param2 stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
        
        NSString* urlAction = @"user/mobile/update";
        NSString* urlPath = [NSString stringWithFormat:@"%@%@",TeamWorkAPIpath_noRest,urlAction];
        
        //NSLog(@"path:%@",urlPath);
        
        NSString* urlPath2 = [urlPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlPath2];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
        
        request.HTTPMethod = @"POST";
        [request setHTTPBody:[param2 dataUsingEncoding:NSUTF8StringEncoding]];
        request.timeoutInterval = 5;
        
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
        
        NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:request
                                                           completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                               //NSLog(@"Response:%@ %@\n", response, error);
                                                               if(error != nil)
                                                               {
                                                                   // alert
                                                                   UIAlertView *alert;
                                                                   alert = [[UIAlertView alloc]
                                                                            initWithTitle:@"There is a problem with the server. Please try update your name later."
                                                                            message:nil
                                                                            delegate:self
                                                                            cancelButtonTitle:@"OK"
                                                                            otherButtonTitles:nil];
                                                                   [alert show];
                                                               }
                                                           }];
        [dataTask resume];
    }

    [self presentLeftMenuViewController:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"My Account";
    
    UIBarButtonItem *menuBtn = [[UIBarButtonItem alloc]
                                initWithTitle:[NSString stringWithFormat:@"%@", [NSString fontAwesomeIconStringForEnum:FABars]]
                                style:UIBarButtonItemStylePlain
                                target:self
                                action:@selector(sideMenuBtnAction)];
    [menuBtn setTitleTextAttributes:@{  NSFontAttributeName: [UIFont fontWithName:kFontAwesomeFamilyName size:18.0f] ,
                                        NSForegroundColorAttributeName: [UIColor whiteColor]
                                        } forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = menuBtn;

    self.profileIV.layer.cornerRadius = self.profileIV.frame.size.width/2;
    self.profileIV.layer.masksToBounds = YES;
    self.profileIV.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.profileIV.layer.borderWidth = 1.0;

    NSString* nameStr = @"Guest";
    NSString* idStr = @"---";
    
    if ([UserSession sharedInstance].userId) {
        if ([UserSession sharedInstance].nickname != nil)
            nameStr = [UserSession sharedInstance].nickname;
        if ([UserSession sharedInstance].userId != nil)
            idStr = [UserSession sharedInstance].userId;
    }
    else {
        [self.nameTextField setEnabled:NO];
    }
    
    [self.nameTextField setText:nameStr];
    [self.idLbl setText:idStr];
    
    name = nameStr;
}

- (void)viewDidAppear:(BOOL)animated {

    [super viewDidAppear:animated];
    
    if ([UserSession sharedInstance].userId) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self getPoint];
    }
}

- (void)getPoint {
    
    UserSession* user = [UserSession sharedInstance];
    NSArray* tokenStr = [user.toKen componentsSeparatedByString:@"-"];
    
    NSString* tokenPassed = user.toKen;
    if ([tokenStr count] > 1) {
        tokenPassed = tokenStr[1];
    }
    
//    tokenPassed = @"02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032";
    
    NSString* param = [NSString stringWithFormat:@"?token=%@&sign=",tokenPassed];
    NSString* urlAction = @"credit/SelectCredit";
    
    NSString* urlPath = [NSString stringWithFormat:@"%@%@%@",LobbyAPIpath,urlAction,param];
    NSString* urlPath2 = [urlPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlPath2];
    
    //    NSLog(@"url:%@",urlPath2);
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data != nil) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            //            NSLog(@"json:%@",json);
            //            NSLog(@"status 2:%d",[json[@"status"] intValue]);
            
            UserSession* user = [UserSession sharedInstance];
            user.getPointSuccess = NO;
            
            if (json[@"status"] != nil && [json[@"status"] intValue] == 1 && json[@"result"] != nil) {
                
                NSString* resultDictStr = json[@"result"];
                NSData *resultDictJsonData = [resultDictStr dataUsingEncoding:NSUTF8StringEncoding];
                id resultDictJson = [NSJSONSerialization JSONObjectWithData:resultDictJsonData options:0 error:nil];
                
                if (resultDictJson != nil) {
                    user.point = [resultDictJson[@"sumCredit"] intValue];
                    user.getPointSuccess = YES;
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:YES];

                UserSession* user = [UserSession sharedInstance];
                if (user.getPointSuccess) {
                    [self.ptLbl setText:[NSString stringWithFormat:@"%d",user.point]];
                }
                else {
                    [self.ptLbl setText:@"---"];
                }
            });
        }
    }];
    [dataTask resume];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark TextField methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString* warningStr = nil;
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    if ((textField.tag == 0) && (newLength > 30)) {
        warningStr = [NSString stringWithFormat:NSLocalizedString(@"Name exceeds\n%d character limit", @""),30];
    }
    
    if (warningStr != nil) {
        // error case
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:warningStr
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                              otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.activeTextField = textField;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField.tag == 0){
        [textField resignFirstResponder];
    }
    return YES;
}

@end
