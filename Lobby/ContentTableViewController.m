//
//  contentTableViewController.m
//  Lobby
//
//  Created by Ivan Wan on 20/2/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import "ContentTableViewController.h"

@interface ContentTableViewController ()

@property (nonatomic, assign) BOOL dataLoaded;

@end

@implementation ContentTableViewController
{
    NSArray *tableData;
    NSString* link;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Showcase";
    
    UIBarButtonItem *menuBtn = [[UIBarButtonItem alloc]
                                initWithTitle:[NSString stringWithFormat:@"%@", [NSString fontAwesomeIconStringForEnum:FABars]]
                                style:UIBarButtonItemStylePlain
                                target:self
                                action:@selector(presentLeftMenuViewController:)];
    [menuBtn setTitleTextAttributes:@{  NSFontAttributeName: [UIFont fontWithName:kFontAwesomeFamilyName size:18.0f] ,
                                        NSForegroundColorAttributeName: [UIColor whiteColor]
                                        } forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = menuBtn;
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 150.0;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
  
    link = AppConfigAPI;
    [self callConfigAPI];
}
/*
- (void)callProductionAPI {
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:AppProd]];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (data != nil) {
            NSLog(@"data:%@",data);
            
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSString* prod = json[@"prod"];

            AppDelegate* appDelegate = LobbyAppDelegate;
            
            link = AppConfigAPI;
            if ([appDelegate.productionVer floatValue] > [prod floatValue]) {
                link = AppConfigAPIDev;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self callConfigAPI];
            });
        }
    }];
    [dataTask resume];
}
*/
- (void)callConfigAPI{
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:link]];
    ///NSLog(@"config:%@",configAPI);
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (data != nil) {
            //NSLog(@"data:%@",data);
            
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            tableData = json[@"games"];

            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self.tableView reloadData];
            });
        }
    }];
    [dataTask resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"GameTableCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    
    NSDictionary* d = [tableData objectAtIndex:indexPath.row];

    UIImageView* iv = (UIImageView*)[cell viewWithTag:10];
    UILabel* titleLbl = (UILabel*)[cell viewWithTag:11];
    UILabel* developerLbl = (UILabel*)[cell viewWithTag:12];
    UILabel* infoLbl = (UILabel*)[cell viewWithTag:13];
    UIButton* b = (UIButton*)[cell viewWithTag:20];
    UILabel* linkLbl = (UILabel*)[cell viewWithTag:14];
    
    titleLbl.text = d[@"name"];
    infoLbl.text  = d[@"description"];
    developerLbl.text = d[@"developer"];
    linkLbl.text = d[@"link"];
    
    b.layer.cornerRadius = 4;
    b.layer.masksToBounds = YES;
    b.layer.borderColor = [UIColor whiteColor].CGColor;
    b.layer.borderWidth = 1.0;
    
    [b addTarget:self action:@selector(cellBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *image = [[UIImage imageNamed:@"web"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [b setImage:image forState:UIControlStateNormal];
    b.tintColor = [UIColor whiteColor];
    
    iv.layer.cornerRadius = 5;    
    [iv setShowActivityIndicatorView:YES];
    [iv setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [iv sd_setImageWithURL:[NSURL URLWithString:d[@"img"]] placeholderImage:nil];
    
    return cell;
}

- (void)cellBtnAction:(id)sender {
    UIButton* b = (UIButton*)sender;
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Open Safari to see more info?" message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UILabel* linkLbl = (UILabel*)[b.superview viewWithTag:14];
        
        NSString* url = linkLbl.text;
        NSURL* nsUrl = [NSURL URLWithString:url];
        UIApplication *application = [UIApplication sharedApplication];
        [application openURL:nsUrl options:@{} completionHandler:nil];
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alertController addAction:ok];
    [alertController addAction:cancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
