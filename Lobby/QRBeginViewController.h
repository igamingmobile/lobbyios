//
//  QRBeginViewController.h
//  Lobby
//
//  Created by Ivan Wan on 21/2/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MTBBarcodeScanner.h"
#import  <FBSDKShareKit/FBSDKShareKit.h>

@interface QRBeginViewController : UIViewController <FBSDKSharingDelegate>

@property (strong, nonatomic) IBOutlet UIView *instructionView;
@property (strong, nonatomic) IBOutlet UIView *previewView;
@property (strong, nonatomic) IBOutlet UIView *sharingImageView;
@property (strong, nonatomic) IBOutlet UIView *sharingImageActualView;

@property (strong, nonatomic) IBOutlet UIImageView *gameImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLbl;
@property (strong, nonatomic) IBOutlet UILabel *amountLbl;

@property (strong, nonatomic) MTBBarcodeScanner* scanner;



@end
