//
//  NewsViewController.m
//  Lobby
//
//  Created by Ivan Wan on 23/1/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import "NewsViewController.h"

@interface NewsViewController ()

@property (nonatomic, assign) BOOL dataLoaded;

@end

@implementation NewsViewController
{
    NSArray *tableData;
    int tableDataCnt;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Notifications";
    
    UIBarButtonItem *menuBtn = [[UIBarButtonItem alloc]
                                initWithTitle:[NSString stringWithFormat:@"%@", [NSString fontAwesomeIconStringForEnum:FABars]]
                                style:UIBarButtonItemStylePlain
                                target:self
                                action:@selector(presentLeftMenuViewController:)];
    [menuBtn setTitleTextAttributes:@{  NSFontAttributeName: [UIFont fontWithName:kFontAwesomeFamilyName size:18.0f] ,
                                        NSForegroundColorAttributeName: [UIColor whiteColor]
                                        } forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = menuBtn;
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 90.0;
    
    NSString* urlAction = @"noti/iGamingGetNotification?page＝1&pageNum=50";
    NSString* urlPath = [NSString stringWithFormat:@"%@%@",LobbyAPIpath,urlAction];
    NSString* urlPath2 = [urlPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    NSString *urlPath2 = [urlPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSURL *url = [NSURL URLWithString:urlPath2];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data != nil) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            tableDataCnt = [json[@"count"] intValue];
            
            NSString* arrayStr = json[@"result"];
            NSData* arrayData = [arrayStr dataUsingEncoding:NSUTF8StringEncoding];
            tableData = [NSJSONSerialization JSONObjectWithData:arrayData options:0 error:nil];
            
            _dataLoaded = YES;
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [self.tableView reloadData];
            });
        }
    }];
    [dataTask resume];
    
    return;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!self.dataLoaded)
        return 0;
    return tableDataCnt;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"NewsTableCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
 
    NSDictionary* d = [tableData objectAtIndex:indexPath.row];
    
    UILabel* titleLbl = (UILabel*)[cell viewWithTag:11];
    UILabel* dateLbl = (UILabel*)[cell viewWithTag:12];
    UILabel* infoLbl = (UILabel*)[cell viewWithTag:13];
    
    titleLbl.text = d[@"notiTitle"];
    infoLbl.text  = d[@"notiDesc"];

    NSString* dateStr = d[@"updateDate"];
    NSArray* dateArray = [dateStr componentsSeparatedByString:@" "];
    dateLbl.text = [dateArray objectAtIndex:0];
    
    return cell;
}

@end
