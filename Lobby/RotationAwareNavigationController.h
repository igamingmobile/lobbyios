//
//  RotationAwareNavigationController.h
//  Lobby
//
//  Created by Ivan Wan on 7/2/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RotationAwareNavigationController : UINavigationController

@end
