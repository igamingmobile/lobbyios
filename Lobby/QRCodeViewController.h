//
//  QRCodeViewController.h
//  Lobby
//
//  Created by Ivan Wan on 21/2/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QRCodeViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *qrImageView;

@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *amountTextField;
@property (strong, nonatomic) IBOutlet UISegmentedControl *gameSegment;

@end
