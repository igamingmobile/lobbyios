//
//  QRCodeViewController.m
//  Lobby
//
//  Created by Ivan Wan on 21/2/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import "QRCodeViewController.h"

@interface QRCodeViewController ()

@end

@implementation QRCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Code Generator";
    
    UIBarButtonItem *menuBtn = [[UIBarButtonItem alloc]
                                initWithTitle:[NSString stringWithFormat:@"%@", [NSString fontAwesomeIconStringForEnum:FABars]]
                                style:UIBarButtonItemStylePlain
                                target:self
                                action:@selector(presentLeftMenuViewController:)];
    [menuBtn setTitleTextAttributes:@{  NSFontAttributeName: [UIFont fontWithName:kFontAwesomeFamilyName size:18.0f] ,
                                        NSForegroundColorAttributeName: [UIColor whiteColor]
                                        } forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = menuBtn;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)generateCode:(id)sender {
    
    NSString* game = @"";
    switch (self.gameSegment.selectedSegmentIndex) {
        case 0:
            game = @"777";
            break;
        case 1:
            game = @"3kingdoms";
            break;
        case 2:
            game = @"happyfarm";
            break;
        default:
            game = @"777";
            break;
    }
    
    NSString* name = self.nameTextField.text;
    if ([name length] == 0) {
        name = @"Player";
    }
    
    NSString* str = [NSString stringWithFormat:@"%@|%@|%@|%@",LobbyQRCheckString,name,self.amountTextField.text,game];

    NSString *str2 = [AESCrypt encrypt:str password:LobbyQREncryptKey];
    
//    NSLog(@"str:%@",str);
//    NSLog(@"encrypt:%@",str2);
    
    self.qrImageView.image = [UIImage mdQRCodeForString:str2
                                                   size:self.qrImageView.bounds.size.width
                                              fillColor:[UIColor purpleColor]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
