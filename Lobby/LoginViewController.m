//
//  LoginViewController.m
//  Lobby
//
//  Created by Ivan Wan on 23/1/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import "LoginViewController.h"



@interface LoginViewController () {
    DGTAuthenticateButton *authButton;
    FBSDKLoginButton *fbloginButton;
}

@end

@implementation LoginViewController

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // add digit login btn
    authButton = [DGTAuthenticateButton buttonWithAuthenticationCompletion:^(DGTSession *session, NSError *error) {
        
//        Digits* digits = [Digits sharedInstance];
//        session = [DGTDebugConfiguration defaultDebugSession];
//        digits.debugOverrides = [[DGTDebugConfiguration alloc] initSuccessStateWithDigitsSession:session];

        if (session.userID) {
            // associate the session userID with your user model
//            NSString *msg = [NSString stringWithFormat:@"Phone number: %@", session.phoneNumber];
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"You are logged in!"
//                                                            message:msg
//                                                           delegate:nil
//                                                  cancelButtonTitle:@"OK"
//                                                  otherButtonTitles:nil];
//            [alert show];
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            [prefs setObject:session.userID forKey:@"digits_userID"];
            [prefs setObject:session.authTokenSecret forKey:@"digits_authTokenSecret"];

            // check if phone's first character is + or other non-digit
            NSString* phone = session.phoneNumber;
            NSString* token = session.authToken;

            //NSCharacterSet *fitstCharSet = [NSCharacterSet characterSetWithCharactersInString:phone];
            //if (![[NSCharacterSet decimalDigitCharacterSet] isSupersetOfSet:fitstCharSet])
            //    phone = [phone substringFromIndex:1];
            [prefs setObject:token forKey:@"digits_authToken"];
            [prefs setObject:phone forKey:@"digits_phoneNumber"];
            [prefs synchronize];

            [UserSession sharedInstance].digits_authToken = session.authToken;
            [UserSession sharedInstance].digits_userID = session.userID;
            [UserSession sharedInstance].digits_authTokenSecret = session.authTokenSecret;
            [UserSession sharedInstance].digits_phoneNumber = phone;
            
            [self loginTggServer];
        } else if (error) {
            NSLog(@"Authentication error: %@", error.localizedDescription);
        }
    }];
    
    authButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.loginBtnView addSubview:authButton];
    
    NSLayoutConstraint *width =[NSLayoutConstraint
                                constraintWithItem:authButton
                                attribute:NSLayoutAttributeWidth
                                relatedBy:0
                                toItem:self.loginBtnView
                                attribute:NSLayoutAttributeWidth
                                multiplier:1.0
                                constant:0];
    NSLayoutConstraint *height =[NSLayoutConstraint
                                 constraintWithItem:authButton
                                 attribute:NSLayoutAttributeHeight
                                 relatedBy:0
                                 toItem:self.loginBtnView
                                 attribute:NSLayoutAttributeHeight
                                 multiplier:1.0
                                 constant:0];
    NSLayoutConstraint *top = [NSLayoutConstraint
                               constraintWithItem:authButton
                               attribute:NSLayoutAttributeTop
                               relatedBy:NSLayoutRelationEqual
                               toItem:self.loginBtnView
                               attribute:NSLayoutAttributeTop
                               multiplier:1.0f
                               constant:0.f];
    NSLayoutConstraint *leading = [NSLayoutConstraint
                                   constraintWithItem:authButton
                                   attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.loginBtnView
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1.0f
                                   constant:0.f];
    [self.loginBtnView addConstraint:width];
    [self.loginBtnView addConstraint:height];
    [self.loginBtnView addConstraint:top];
    [self.loginBtnView addConstraint:leading];
    
    fbloginButton = [[FBSDKLoginButton alloc] init];
    fbloginButton.delegate = self;
    
    fbloginButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.fbLoginBtnView addSubview:fbloginButton];
    
    NSLayoutConstraint *width2 =[NSLayoutConstraint
                                constraintWithItem:fbloginButton
                                attribute:NSLayoutAttributeWidth
                                relatedBy:0
                                toItem:self.fbLoginBtnView
                                attribute:NSLayoutAttributeWidth
                                multiplier:1.0
                                constant:0];
//    NSLayoutConstraint *height2 =[NSLayoutConstraint
//                                 constraintWithItem:fbloginButton
//                                 attribute:NSLayoutAttributeHeight
//                                 relatedBy:0
//                                 toItem:self.fbLoginBtnView
//                                 attribute:NSLayoutAttributeHeight
//                                 multiplier:1.0
//                                 constant:0];
    NSLayoutConstraint *top2 = [NSLayoutConstraint
                               constraintWithItem:fbloginButton
                               attribute:NSLayoutAttributeTop
                               relatedBy:NSLayoutRelationEqual
                               toItem:self.fbLoginBtnView
                               attribute:NSLayoutAttributeTop
                               multiplier:1.0f
                               constant:0.f];
    NSLayoutConstraint *leading2 = [NSLayoutConstraint
                                   constraintWithItem:fbloginButton
                                   attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.fbLoginBtnView
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1.0f
                                   constant:0.f];
    [self.fbLoginBtnView addConstraint:width2];
//    [self.fbLoginBtnView addConstraint:height2];
    [self.fbLoginBtnView addConstraint:top2];
    [self.fbLoginBtnView addConstraint:leading2];
    
    [authButton setHidden:YES];
    [fbloginButton setHidden:YES];
    
    CGSize size = CGSizeMake(25.0, 25.0);
    
    UIImage* i1 = [UIImage imageWithIcon:@"fa-phone-square" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] andSize:size];
    [self.phoneloginBtn setImage:i1 forState:UIControlStateNormal];

    UIImage* i2 = [UIImage imageWithIcon:@"fa-facebook-official" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] andSize:size];
    [self.fbLoginBtn setImage:i2 forState:UIControlStateNormal];
}

- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error {
    NSLog(@"token:%@",result.token);
    NSLog(@"cancel:%d",result.isCancelled);
    
    if (!result.isCancelled && result.token != nil) {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setObject:result.token.tokenString forKey:@"fb_token"];
        [prefs setObject:result.token.userID forKey:@"fb_userID"];
        
        [prefs synchronize];
        
        [UserSession sharedInstance].fb_userID = result.token.userID;
        
        [self loginTggServer];
    }
}

- (void)loginTggServer {
    
    NSString* token = @"";
    NSString* phone = @"";
    
    Digits* digits = [Digits sharedInstance];
    if ([UserSession sharedInstance].digits_authToken != nil) {
        token = [UserSession sharedInstance].digits_authToken;
        phone = digits.session.phoneNumber;
        
        // check if phone's first character is + or other non-digit
        //    NSCharacterSet *fitstCharSet = [NSCharacterSet characterSetWithCharactersInString:phone];
        //    if (![[NSCharacterSet decimalDigitCharacterSet] isSupersetOfSet:fitstCharSet])
        //        phone = [phone substringFromIndex:1];
    }
    else if ([UserSession sharedInstance].fb_userID)
        token = [UserSession sharedInstance].fb_userID;
    
//    DGTSession *session = [DGTDebugConfiguration defaultDebugSession];
//    digits.debugOverrides = [[DGTDebugConfiguration alloc] initSuccessStateWithDigitsSession:session];
    
    NSString* time = (NSString*)[NSDate date];
    NSString* sign = @"";

    NSString* param = [NSString stringWithFormat:@"phone=%@&time=%@&token=%@&sign=%@",phone,time,token,sign];
    NSString* param2 = [param stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    param2 = [param2 stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    
    NSString* urlAction = @"user/mobile/login";
    NSString* urlPath = [NSString stringWithFormat:@"%@%@",TeamWorkAPIpath_noRest,urlAction];
    
//    NSLog(@"loginTggServer | path :%@",urlPath);
//    NSLog(@"loginTggServer | param:%@",param2);
    
    NSString* urlPath2 = [urlPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlPath2];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    request.HTTPMethod = @"POST";
    [request setHTTPBody:[param2 dataUsingEncoding:NSUTF8StringEncoding]];
    request.timeoutInterval = 5;
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:request
                                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                           //NSLog(@"Response:%@ %@\n", response, error);
                                                           if(error == nil)
                                                           {
                                                               //NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                                                               //NSLog(@"Data = %@",text);
                                                           }
                                                           if (data != nil) {
                                                               NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                               NSString* dictStr = json[@"result"];
                                                               
                                                               NSData *data2 = [dictStr dataUsingEncoding:NSUTF8StringEncoding];
                                                               id resultDict;
                                                               if (data2) {
                                                                   resultDict = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
                                                               }
                                                               else {
                                                                   UIAlertView *alert;
                                                                   alert = [[UIAlertView alloc]
                                                                            initWithTitle:@"There is a problem with the server. Please try login again later."
                                                                            message:nil
                                                                            delegate:self
                                                                            cancelButtonTitle:@"OK"
                                                                            otherButtonTitles:nil];
                                                                   [alert show];
                                                                   
                                                                   [self performSegueWithIdentifier: @"login_menu_segue" sender: self];
                                                               }
                                                               // has user data
                                                               if (resultDict) {
                                                                   NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                                                   [prefs setObject:resultDict[@"userId"] forKey:@"userId"];
                                                                   [prefs setObject:resultDict[@"phone"] forKey:@"phone"];
                                                                   [prefs setObject:resultDict[@"nickname"] forKey:@"nickname"];
                                                                   [prefs setObject:resultDict[@"email"] forKey:@"email"];
                                                                   [prefs setObject:resultDict[@"toKen"] forKey:@"toKen"];
                                                                   [prefs setObject:resultDict[@"customId"] forKey:@"customId"];
                                                                   [prefs setObject:resultDict[@"sex"] forKey:@"sex"];
                                                                   [prefs synchronize];
                                                                   
                                                                   UserSession* user = [UserSession sharedInstance];
                                                                   user.userId = resultDict[@"userId"];
                                                                   user.phone = resultDict[@"phone"];
                                                                   user.nickname = resultDict[@"nickname"];
                                                                   user.email = resultDict[@"email"];
                                                                   user.toKen = resultDict[@"toKen"];
                                                                   user.customId = resultDict[@"customId"];
                                                                   user.sex = resultDict[@"sex"];
                                                                   
                                                                   // get user credits
                                                                   
                                                                   [self performSegueWithIdentifier: @"login_menu_segue" sender: self];
                                                               }
                                                               else {
                                                                   [self performSegueWithIdentifier:@"login_setup_segue" sender:self];
                                                               }
                                                           }
                                                       }];
    [dataTask resume];
}

- (IBAction)tnCAction:(id)sender {
    [self performSegueWithIdentifier: @"loginTNCsegue" sender: self];
}

//- (IBAction)signInAction:(id)sender {
//    [self performSegueWithIdentifier:@"login_setup_segue" sender:self];
//}

- (IBAction)phoneloginBtnPressed:(id)sender {
    
    [authButton sendActionsForControlEvents: UIControlEventTouchUpInside];
}

- (IBAction)fbloginBtnPressed:(id)sender {
    
    [fbloginButton sendActionsForControlEvents: UIControlEventTouchUpInside];
}

- (IBAction)skipSignInAction:(id)sender {
    [self performSegueWithIdentifier: @"login_menu_segue" sender: self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
