//
//  leftMenuViewController.m
//  Lobby
//
//  Created by Ivan Wan on 23/1/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import "leftMenuViewController.h"


@interface leftMenuViewController ()

@end

@implementation leftMenuViewController

- (void)updateUserInfo {
    
    NSString* nameStr = @"Guest";
    //    NSString* idStr = @"";
    NSString* ptStr = @"";
    
    UserSession* usr = [UserSession sharedInstance];
    if (usr.userId) {
        if (usr.nickname != nil)
            nameStr = [UserSession sharedInstance].nickname;
        //        if ([UserSession sharedInstance].userId != nil) {
        //            idStr = [UserSession sharedInstance].userId;
        //            idStr = [NSString stringWithFormat:@"ID: %@",idStr];
        //        }
        if (usr.getPointSuccess) {
            [self.ptLbl setHidden:NO];
            ptStr = [NSString stringWithFormat:@"Point: %d",usr.point];
        }
        else {
            [self.ptLbl setHidden:YES];
        }
    }
//    else {
//        [self.accountSetupBkgrdBtn setHidden:YES];
//        [self.accountSetupBtn setHidden:YES];
//    }
    
    [self.nameLbl setText:nameStr];
    //    [self.idLbl setText:idStr];
    [self.ptLbl setText:ptStr];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self updateUserInfo];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.accountSetupBtn.titleLabel setFont:[UIFont fontWithName:kFontAwesomeFamilyName size:17.f]];
    [self.accountSetupBtn setTitle:[NSString stringWithFormat:@"%@", [NSString fontAwesomeIconStringForEnum:FACog]] forState:UIControlStateNormal];

    CGSize size = CGSizeMake(25.0, 25.0);
    
    UIImage* i1 = [UIImage imageWithIcon:@"fa-star" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] andSize:size];
    [self.btnGames setImage:i1 forState:UIControlStateNormal];
    
    UIImage* i2 = [UIImage imageWithIcon:@"fa-user" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] andSize:size];
    [self.btnInvite setImage:i2 forState:UIControlStateNormal];
    
    UIImage* i3 = [UIImage imageWithIcon:@"fa-newspaperO" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] andSize:size];
    [self.btnNews setImage:i3 forState:UIControlStateNormal];
    
    UIImage* i4 = [UIImage imageWithIcon:@"fa-file" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] andSize:size];
    [self.btnTerms setImage:i4 forState:UIControlStateNormal];
    
    UIImage* i5 = [UIImage imageWithIcon:@"fa-calculator" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] andSize:size];
    [self.btnTracker setImage:i5 forState:UIControlStateNormal];
    
    UIImage* i6 = [UIImage imageWithIcon:@"fa-camera" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] andSize:size];
    [self.btnQRTest setImage:i6 forState:UIControlStateNormal];
    
    UIImage* i7 = [UIImage imageWithIcon:@"fa-qrcode" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] andSize:size];
    [self.btnCodeGen setImage:i7 forState:UIControlStateNormal];
    
    self.profileIV.layer.cornerRadius = self.profileIV.frame.size.width/2;
    self.profileIV.layer.masksToBounds = YES;
    self.profileIV.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.profileIV.layer.borderWidth = 1.0;
    
    [self.btnNews setHidden:YES];
    
    if (!EnableQRDemo) {
        [self.btnQRTest setHidden:YES];
        [self.btnCodeGen setHidden:YES];
    }
}

- (IBAction)btnTrackerAtn:(id)sender {
    AppDelegate* appDelegate = LobbyAppDelegate;
    if (!appDelegate.trackerViewController)
        appDelegate.trackerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TrackerViewNavController"];
    [self.sideMenuViewController setContentViewController:appDelegate.trackerViewController animated:YES];
    
    [self.sideMenuViewController hideMenuViewController];
}

- (IBAction)btnAccountAtn:(id)sender {
    AppDelegate* appDelegate = LobbyAppDelegate;
    if (!appDelegate.accountViewController)
        appDelegate.accountViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AccountViewNavController"];
    [self.sideMenuViewController setContentViewController:appDelegate.accountViewController animated:YES];

    [self.sideMenuViewController hideMenuViewController];
}
- (IBAction)btnGameAtn:(id)sender {
    AppDelegate* appDelegate = LobbyAppDelegate;
    if (!appDelegate.contentTableViewController)
        appDelegate.contentTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ContentTableViewNavController"];
    [self.sideMenuViewController setContentViewController:appDelegate.contentTableViewController animated:YES];
    
    [self.sideMenuViewController hideMenuViewController];
}

- (IBAction)btnInviteAtn:(id)sender {
    AppDelegate* appDelegate = LobbyAppDelegate;
    if (!appDelegate.inviteViewController)
        appDelegate.inviteViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"InviteViewNavController"];
    [self.sideMenuViewController setContentViewController:appDelegate.inviteViewController animated:YES];
    
    [self.sideMenuViewController hideMenuViewController];
}
- (IBAction)btnNewsAtn:(id)sender {
    AppDelegate* appDelegate = LobbyAppDelegate;
    if (!appDelegate.newsViewController)
        appDelegate.newsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsViewNavController"];
    [self.sideMenuViewController setContentViewController:appDelegate.newsViewController animated:YES];
    
    [self.sideMenuViewController hideMenuViewController];
}
- (IBAction)btnTermsAtn:(id)sender {
    AppDelegate* appDelegate = LobbyAppDelegate;
    if (!appDelegate.termsViewController)
        appDelegate.termsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsViewNavController"];
    [self.sideMenuViewController setContentViewController:appDelegate.termsViewController animated:YES];
    
    [self.sideMenuViewController hideMenuViewController];
}

- (IBAction)btnQRAtn:(id)sender {
    AppDelegate* appDelegate = LobbyAppDelegate;
    if (!appDelegate.qrBeginViewController)
        appDelegate.qrBeginViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"QRBeginNavController"];
    [self.sideMenuViewController setContentViewController:appDelegate.qrBeginViewController animated:YES];
    
    [self.sideMenuViewController hideMenuViewController];
}

- (IBAction)btnCodeAtn:(id)sender {
    UINavigationController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"QRCodeNavController"];
    [self.sideMenuViewController setContentViewController:vc animated:YES];
    [self.sideMenuViewController hideMenuViewController];    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
