//
//  TrackerViewController.m
//  Lobby
//
//  Created by Ivan Wan on 14/2/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import "TrackerViewController.h"
#import "TrackerDetaillViewController.h"

#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

#define unspecifedAlpha 0.5

@interface TrackerViewController ()

@end

@implementation TrackerViewController
{
    NSDateFormatter *dateFormat;
    NSArray *tableData;
    float balance;
    NSString* entryID;
    
    NSTimer* soundTimer;
}

- (void)loadData {
    NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString* filePath = [documentsPath stringByAppendingPathComponent:slotRecordFileName];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    
    if (fileExists) {
        NSDictionary *d = [NSDictionary dictionaryWithContentsOfFile:filePath];
        tableData = [[d allValues] sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSDate *first = [(NSDictionary*)a objectForKey:@"date"];
            NSDate *second = [(NSDictionary*)b objectForKey:@"date"];
            return [second compare:first];
        }];
        //NSLog(@"a:%@",tableData);
        
        // calculate balance
        float total = 0;
        for (NSDictionary* d in tableData) {
            float f = [d[@"amount"] floatValue];
            total += f;
        }
        balance = total;
        
        self.balanceLbl.formatBlock = ^NSString* (CGFloat value) {
            if (total > 0) {
                return [NSString stringWithFormat: @"$%.f", value];
            }
            else {
                return [NSString stringWithFormat: @"-$%.f", value * -1];
            }
        };
        
//        if (total > 0)
//            [self.balanceLbl setText:[NSString stringWithFormat:@"$%.f",total]];
//        else
//            [self.balanceLbl setText:[NSString stringWithFormat:@"-$%.f",total * -1]];
        
        if ([tableData count] > 0)
            [self performSelector:@selector(animateLabel) withObject:nil afterDelay:0.5];
    }
}

- (void)animateLabel {
    [self.balanceLbl countFromZeroTo:balance withDuration:1.5f];
    soundTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                  target:self
                                                selector:@selector(playSound)
                                                userInfo:nil
                                                 repeats:NO];
}

- (void)playSound {
    
    //Retrieve audio file
    NSString *path  = [[NSBundle mainBundle] pathForResource:@"bell" ofType:@"m4a"];
    NSURL *pathURL = [NSURL fileURLWithPath : path];
    
    SystemSoundID audioEffect;
    AudioServicesCreateSystemSoundID((__bridge CFURLRef) pathURL, &audioEffect);
    AudioServicesPlaySystemSound(audioEffect);
    
    // call the following function when the sound is no longer used
    // (must be done AFTER the sound is done playing)
//    AudioServicesDisposeSystemSoundID(audioEffect);
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [soundTimer invalidate];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadData];
    
    if ([tableData count] > 0) {
        [self.emptyView setHidden:YES];
        [self.tableView setHidden:NO];
        [self.balanceView setHidden:NO];
    }
    else {
        [self.emptyView setHidden:NO];
        [self.tableView setHidden:YES];
        [self.balanceView setHidden:YES];
    }
    
    [self.tableView reloadData];
    [self.balanceLbl setText:@"$0"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.title = @"Slot Tracker";
    self.navigationItem.titleView = self.titleView;
    
    UIBarButtonItem *menuBtn = [[UIBarButtonItem alloc]
                                initWithTitle:[NSString stringWithFormat:@"%@", [NSString fontAwesomeIconStringForEnum:FABars]]
                                style:UIBarButtonItemStylePlain
                                target:self
                                action:@selector(presentLeftMenuViewController:)];
    [menuBtn setTitleTextAttributes:@{  NSFontAttributeName: [UIFont fontWithName:kFontAwesomeFamilyName size:18.0f] ,
                                        NSForegroundColorAttributeName: [UIColor whiteColor]
                                        } forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = menuBtn;
    
    UIBarButtonItem *plusBtn = [[UIBarButtonItem alloc]
                                initWithTitle:[NSString stringWithFormat:@"%@", [NSString fontAwesomeIconStringForEnum:FAPlus]]
                                style:UIBarButtonItemStylePlain
                                target:self
                                action:@selector(addRecord)];
    [plusBtn setTitleTextAttributes:@{  NSFontAttributeName: [UIFont fontWithName:kFontAwesomeFamilyName size:18.0f] ,
                                        NSForegroundColorAttributeName: [UIColor whiteColor]
                                        } forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = plusBtn;

    self.tableView.backgroundColor = [UIColor clearColor];
    
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 150.0;
}

- (IBAction)addRecord {
    entryID = nil;
    [self performSegueWithIdentifier:@"tracker_detail_segue" sender:self];
}

- (void)editRecord:(id)sender {
    UIButton* b = (UIButton*)sender;
    //btnIdx = b.tag;
    
    UILabel* l = (UILabel*)[[b superview] viewWithTag:30];
    entryID = l.text;
    
    [self performSegueWithIdentifier:@"tracker_detail_segue" sender:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableData count];
}

//- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//    NSDictionary* d = (NSDictionary*)[tableData objectAtIndex:indexPath.row];
//    NSString* note = d[@"note"];
//    if ([note length] == 0) {
//        return 145;
//    }
//    return 175;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"TrackerCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];

    UILabel* dateTitleLbl = (UILabel*)[cell viewWithTag:21];
    UILabel* locTitleLbl = (UILabel*)[cell viewWithTag:22];
    UILabel* noteTitleLbl = (UILabel*)[cell viewWithTag:23];
    
    UIButton* editBtnBig = (UIButton*)[cell viewWithTag:9];
    UIButton* editBtn = (UIButton*)[cell viewWithTag:10];
    UILabel* dateLbl = (UILabel*)[cell viewWithTag:11];
    UILabel* nameLbl = (UILabel*)[cell viewWithTag:12];
    UILabel* balanceLbl = (UILabel*)[cell viewWithTag:13];
    UILabel* locLbl = (UILabel*)[cell viewWithTag:14];
    UILabel* idLbl = (UILabel*)[cell viewWithTag:30];
    UILabel* noteView = (UILabel*)[cell viewWithTag:15];
    UIView* blurView = (UITextView*)[cell viewWithTag:40];
    
    UIFont *font1;
    NSDictionary *dict1;

    blurView.layer.cornerRadius = 5;
    blurView.layer.masksToBounds = YES;
    
    // edit button
    font1 = [UIFont fontWithName:kFontAwesomeFamilyName size:20.0f];
    dict1 = @{NSFontAttributeName:font1, NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", [NSString fontAwesomeIconStringForEnum:FAPencilSquare]] attributes:dict1]];
    [editBtn setAttributedTitle:attString forState:UIControlStateNormal];
    [editBtn addTarget:self action:@selector(editRecord:) forControlEvents:UIControlEventTouchUpInside];
    [editBtn setTag:indexPath.row];

    [editBtnBig addTarget:self action:@selector(editRecord:) forControlEvents:UIControlEventTouchUpInside];
    [editBtnBig setTag:indexPath.row];
    
    // date label
    font1 = [UIFont fontWithName:kFontAwesomeFamilyName size:14.0f];
    dict1 = @{NSFontAttributeName:font1, NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    NSMutableAttributedString *attStringDate = [[NSMutableAttributedString alloc] init];
    [attStringDate appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", [NSString fontAwesomeIconStringForEnum:FACalendarO]] attributes:dict1]];
    [dateTitleLbl setAttributedText:attStringDate];
    
    // location label
    NSMutableAttributedString *attStringLoc = [[NSMutableAttributedString alloc] init];
    [attStringLoc appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", [NSString fontAwesomeIconStringForEnum:FAMapMarker]] attributes:dict1]];
    [locTitleLbl setAttributedText:attStringLoc];

    // note label
    NSMutableAttributedString *attStringNote = [[NSMutableAttributedString alloc] init];
    [attStringNote appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", [NSString fontAwesomeIconStringForEnum:FAFileTextO]] attributes:dict1]];
    [noteTitleLbl setAttributedText:attStringNote];
    
    NSDictionary* d = (NSDictionary*)[tableData objectAtIndex:indexPath.row];
    
    NSDate* date = (NSDate*)d[@"date"];
    [dateLbl setText:[dateFormat stringFromDate:date]];
    
    NSString* amount = d[@"amount"];
    if ([[amount substringToIndex:1] isEqualToString:@"-"]) {
        amount = [NSString stringWithFormat:@"-$%@", [amount substringFromIndex:1]];
    }
    else {
        amount = [NSString stringWithFormat:@"$%@",amount];
    }
    [balanceLbl setText:amount];
    
    NSString* name = d[@"name"];
    if ([name length] == 0) {
        name = @"Unspecified Slot";
        nameLbl.alpha = unspecifedAlpha;
    }
    else {
        nameLbl.alpha = 1.0;
    }
    [nameLbl setText:name];

    NSString* location = d[@"location"];
    if ([location length] == 0) {
        location = @"Unspecified Location";
        locLbl.alpha = unspecifedAlpha;
    }
    else {
        locLbl.alpha = 1.0;
    }
    [locLbl setText:location];
    
    NSString* note = d[@"note"];
    [noteView setText:note];
    if ([note length] == 0) {
        [noteView setHidden:YES];
        [noteTitleLbl setHidden:YES];
    }
    else {
        [noteView setHidden:NO];
        [noteTitleLbl setHidden:NO];
    }
    [idLbl setText:d[@"id"]];

    return cell;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"tracker_detail_segue"] ) {
        UIViewController *uv = segue.destinationViewController;
        
        if ([uv isKindOfClass:[TrackerDetaillViewController class]]) {
            TrackerDetaillViewController* vc = (TrackerDetaillViewController*)uv;
            vc.idStr = entryID;
        }
    }
}

@end
