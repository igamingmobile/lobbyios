//
//  TermsViewController.m
//  Lobby
//
//  Created by Ivan Wan on 23/1/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import "TermsViewController.h"

@interface TermsViewController ()

@end

@implementation TermsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Terms & Conditions";
    
    // if side menu created
    if (self.sideMenuViewController) {
        UIBarButtonItem *menuBtn = [[UIBarButtonItem alloc]
                                    initWithTitle:[NSString stringWithFormat:@"%@", [NSString fontAwesomeIconStringForEnum:FABars]]
                                    style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(presentLeftMenuViewController:)];
        [menuBtn setTitleTextAttributes:@{  NSFontAttributeName: [UIFont fontWithName:kFontAwesomeFamilyName size:18.0f] ,
                                            NSForegroundColorAttributeName: [UIColor whiteColor]
                                            } forState:UIControlStateNormal];
        self.navigationItem.leftBarButtonItem = menuBtn;
    }
//    else {
//        UIBarButtonItem *closeBtn = [[UIBarButtonItem alloc]
//                                    initWithTitle:[NSString stringWithFormat:@"%@", [NSString fontAwesomeIconStringForEnum:FATimes]]
//                                    style:UIBarButtonItemStylePlain
//                                    target:self
//                                    action:@selector(dismissSelf)];
//        [closeBtn setTitleTextAttributes:@{  NSFontAttributeName: [UIFont fontWithName:kFontAwesomeFamilyName size:18.0f] ,
//                                            NSForegroundColorAttributeName: [UIColor whiteColor]
//                                            } forState:UIControlStateNormal];
//        self.navigationItem.leftBarButtonItem = closeBtn;
//    }
    
    UIBarButtonItem *infoBtn = [[UIBarButtonItem alloc]
                                initWithTitle:[NSString stringWithFormat:@"%@", [NSString fontAwesomeIconStringForEnum:FAInfo]]
                                style:UIBarButtonItemStylePlain
                                target:self
                                action:@selector(showInfo)];
    [infoBtn setTitleTextAttributes:@{  NSFontAttributeName: [UIFont fontWithName:kFontAwesomeFamilyName size:18.0f] ,
                                        NSForegroundColorAttributeName: [UIColor whiteColor]
                                        } forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = infoBtn;
    
}

- (void)showInfo {
    
    // get version
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    
    // UI elements localized
    NSString* verStr = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Version", @""),majorVersion];
    NSString* copyStr = @"Copyright 2017 TGG.\nAll rights reserved.";
    
    NSString* msg = [NSString stringWithFormat:@"%@\n\n%@",verStr,copyStr];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"TGG Lobby iOS" message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

//- (void)dismissSelf {
//    [self dismissViewControllerAnimated:YES completion:nil];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
