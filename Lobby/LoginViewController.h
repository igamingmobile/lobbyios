//
//  LoginViewController.h
//  Lobby
//
//  Created by Ivan Wan on 23/1/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <DigitsKit/DigitsKit.h>

@interface LoginViewController : PortraitViewController <FBSDKLoginButtonDelegate>

@property (strong, nonatomic) IBOutlet UIButton *phoneloginBtn;
@property (strong, nonatomic) IBOutlet UIButton *fbLoginBtn;

@property (strong, nonatomic) IBOutlet UIView *loginBtnView;
@property (strong, nonatomic) IBOutlet UIView *fbLoginBtnView;

- (IBAction)tnCAction:(id)sender;

- (IBAction)phoneloginBtnPressed:(id)sender;
- (IBAction)fbloginBtnPressed:(id)sender;

- (IBAction)skipSignInAction:(id)sender;
@end
