//
//  LandscapeViewController.h
//  Lobby
//
//  Created by Ivan Wan on 3/2/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LandscapeWebViewController : UIViewController <UIWebViewDelegate>

@property (strong, nonatomic) UIWebView *webView;

- (void)loadWebRequest:(NSURLRequest*)request;

@end
