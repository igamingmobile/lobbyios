//
//  QRBeginViewController.m
//  Lobby
//
//  Created by Ivan Wan on 21/2/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import "QRBeginViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface QRBeginViewController ()

@end

@implementation QRBeginViewController {
    NSString* scannedStr;
    NSString* decryptStr;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    scannedStr = @"";
    [self startScan];
}

- (void)shareAction {
//    [self.view bringSubviewToFront:self.previewView];
    
    NSArray* data = [decryptStr componentsSeparatedByString:@"|"];
    NSString* name = data[1];
    NSString* amount = data[2];
    NSString* game = data[3];
    NSString* gameImgFile;
    if ([game isEqualToString:@"777"]) {
        gameImgFile = @"777.jpg";
    } else if ([game isEqualToString:@"3kingdoms"]) {
        gameImgFile = @"threeKingdoms.jpg";
    }
    else {
        gameImgFile = @"happyfarm.jpg";
    }
    
    [self.amountLbl setText:[NSString stringWithFormat:@"$%@",amount]];
    [self.nameLbl setText:name];
    [self.gameImageView setImage:[UIImage imageNamed:gameImgFile]];
    
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(shareAction2) userInfo:nil repeats:NO];
}

- (void)shareAction2 {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    UIView* drawView = self.sharingImageActualView;
    
    UIGraphicsBeginImageContextWithOptions(drawView.bounds.size, drawView.opaque, 0.0f);
    [drawView drawViewHierarchyInRect:drawView.bounds afterScreenUpdates:NO];
    UIImage *snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

//    UIGraphicsBeginImageContextWithOptions(drawView.bounds.size, drawView.opaque, 0.0);
//    [drawView.layer renderInContext:UIGraphicsGetCurrentContext()];
//    UIImage * snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();

    
    FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
    photo.image = snapshotImageFromMyView;
    photo.userGenerated = NO;
    photo.imageURL = [NSURL URLWithString:CompanyURL];
    
    FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
    content.photos = @[photo];
    
//    [FBSDKShareDialog showFromViewController:self withContent:content delegate:self];
    
    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    dialog.delegate = self;
    dialog.fromViewController = self;
    dialog.shareContent = content;
    dialog.mode = FBSDKShareDialogModeAutomatic; // if you don't set this before canShow call, canShow would always return YES
    if (![dialog canShow]) {
//        NSLog(@"no show");
        // fallback presentation when there is no FB app
//        dialog.mode = FBSDKShareDialogModeFeedBrowser;
    }
    [dialog show];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"QR Sharing";

    UIBarButtonItem *menuBtn = [[UIBarButtonItem alloc]
                                initWithTitle:[NSString stringWithFormat:@"%@", [NSString fontAwesomeIconStringForEnum:FABars]]
                                style:UIBarButtonItemStylePlain
                                target:self
                                action:@selector(presentLeftMenuViewController:)];
    [menuBtn setTitleTextAttributes:@{  NSFontAttributeName: [UIFont fontWithName:kFontAwesomeFamilyName size:18.0f] ,
                                        NSForegroundColorAttributeName: [UIColor whiteColor]
                                        } forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = menuBtn;
    
    [self.view sendSubviewToBack:self.sharingImageView];
    
    self.instructionView.layer.cornerRadius = 5;
    self.instructionView.layer.masksToBounds = YES;

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.instructionView setHidden:YES];
    
    self.scanner = [[MTBBarcodeScanner alloc] initWithPreviewView:self.previewView];
    __weak QRBeginViewController *weakSelf = self;
    self.scanner.didStartScanningBlock = ^{
        //        NSLog(@"The scanner started scanning! We can now hide any activity spinners.");
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
            [weakSelf.instructionView setHidden:NO];
        });
    };
    
    [self startScan];
    
    UIColor *color = [UIColor whiteColor];
    self.gameImageView.layer.shadowColor = [color CGColor];
    self.gameImageView.layer.shadowRadius = 10.0f;
    self.gameImageView.layer.shadowOpacity = .9;
    self.gameImageView.layer.shadowOffset = CGSizeZero;
    self.gameImageView.layer.masksToBounds = NO;
    
//    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(shareAction) userInfo:nil repeats:NO];
//    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(shareAction2) userInfo:nil repeats:NO];
}

- (void)startScan {
    
    [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
        if (success) {
            
            NSError *error = nil;
            [self.scanner startScanningWithResultBlock:^(NSArray *codes) {
                AVMetadataMachineReadableCodeObject *code = [codes firstObject];
                NSLog(@"Found code: %@", code.stringValue);
                
//                if ([code.stringValue hasPrefix:LobbyQRKey] && ![sharingStr isEqualToString: code.stringValue]) {
                if (![scannedStr isEqualToString: code.stringValue]) {
                    scannedStr = code.stringValue;
                    
                    decryptStr = [AESCrypt decrypt:scannedStr password:LobbyQREncryptKey];
//                    NSLog(@"de:%@",decryptStr);
                    
                    if ([decryptStr hasPrefix:LobbyQRCheckString]) {
                        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                        [self.instructionView setHidden:YES];
                        [self shareAction];
                        [self.scanner stopScanning];
                    }
                }
                else {
                    // do nothing
                }
            } error:&error];
            
        } else {
            // The user denied access to the camera
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results {
    [self startScan];
    
}

- (void)sharer:	(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error {
    NSLog(@"error:%@",error);
    [self startScan];
}

- (void) sharerDidCancel:(id<FBSDKSharing>)sharer {
    [self startScan];
}

@end
