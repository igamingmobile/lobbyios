//
//  ViewController.h
//  Lobby
//
//  Created by Ivan Wan on 20/1/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : PortraitViewController

- (IBAction)unwindToBaseViewController:(UIStoryboardSegue *)unwindSegue;

@end

