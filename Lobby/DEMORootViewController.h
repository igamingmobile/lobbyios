//
//  DEMORootViewController.h
//  Lobby
//
//  Created by Ivan Wan on 23/1/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"

@interface DEMORootViewController : RESideMenu <RESideMenuDelegate>

- (IBAction)unwind;

@end
