//
//  AccountViewController.h
//  Lobby
//
//  Created by Ivan Wan on 23/1/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *profileIV;

@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UILabel *idLbl;
@property (strong, nonatomic) IBOutlet UILabel *ptLbl;

@end
