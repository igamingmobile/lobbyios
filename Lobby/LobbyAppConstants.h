//
//  LobbyAppConstants.h
//  Lobby
//
//  Created by Ivan Wan on 23/1/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#ifndef LobbyAppConstants_h
#define LobbyAppConstants_h

#define CompanyURL                  @"https://tgginteractive.com/"

#define AppConfigAPI                @"https://raw.githubusercontent.com/TGGiOS/Lobby/master/tgglobby.json"

//#define AppProd                     @"https://raw.githubusercontent.com/TGGiOS/Lobby/master/prod.json"
//#define AppConfigAPIDev             @"https://raw.githubusercontent.com/TGGiOS/Lobby/master/tgglobbyDev.json"

// Account to update the above data in gitHub
//
// Website:     https://github.com/
// Login:       ivanwan@tgginteractive.com (or TGGiOS)
// Pwd:         hongkong607


#define EnableQRDemo                NO
#define LobbyQRCheckString          @"LOBBYQR"
#define LobbyQREncryptKey           @"L033YQR"

#define LobbyAPPID                  1214778645
#define AppLink                     @"http://itunes.apple.com/app/id1214778645"

#define LobbyAPIpath                @"http://52.74.85.113:8888/credit-rest/rest/"
#define TeamWorkAPIpath             @"http://teamworkapi.tgginteractive.com/tgg-rest/rest/"
//#define TeamWorkAPIpath_noRest      @"http://teamworkapi.tgginteractive.com/tgg-rest/"
#define TeamWorkAPIpath_noRest      @"http://52.74.85.113:8888/tgg-rest/"


#define slotRecordFileName          @"slotrecord"

#define DailyCheckInPoint           @"100"
#define IOSUpdatePointChannel       @"12991"

#define LobbyAppDelegate (AppDelegate *)[[UIApplication sharedApplication] delegate]

#define  ScreenWidth    [[UIScreen mainScreen] bounds].size.width
#define  ScreenHeight   [[UIScreen mainScreen] bounds].size.height

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#endif /* LobbyAppConstants_h */
