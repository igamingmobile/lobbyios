//
//  DEMORootViewController.m
//  Lobby
//
//  Created by Ivan Wan on 23/1/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import "DEMORootViewController.h"

@interface DEMORootViewController ()

@end

@implementation DEMORootViewController

- (IBAction)unwind {
    [self performSegueWithIdentifier:@"unwindToBaseViewController" sender:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    AppDelegate* appDelegate = LobbyAppDelegate;
    appDelegate.demoRootViewController = self;    
}

- (BOOL)isSameDayWithDate1:(NSDate*)date1 date2:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day] == [comp2 day] && [comp1 month] == [comp2 month] && [comp1 year]  == [comp2 year];
}

- (void)checkPointInfo {

    // server side will do checking
    [self callIncrementDailyPointAPI];
//    [self callCheckPointAPI];
    
    /*
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSDate* lastCheckInPointDate = (NSDate*)[prefs objectForKey:@"lastCheckInPointDate"];
    
    if (lastCheckInPointDate == nil || ![self isSameDayWithDate1:[NSDate date] date2:lastCheckInPointDate]) {
        [self callIncrementDailyPointAPI];
    }
    else {
        [self callCheckPointAPI];
    }
    */
}

- (void)callCheckPointAPI {
    
    UserSession* user = [UserSession sharedInstance];
    NSArray* tokenStr = [user.toKen componentsSeparatedByString:@"-"];
    
    NSString* tokenPassed = user.toKen;
    if ([tokenStr count] > 1) {
        tokenPassed = tokenStr[1];
    }
    
    NSString* param = [NSString stringWithFormat:@"?token=%@&sign=",tokenPassed];
    NSString* urlAction = @"credit/SelectCredit";
    
    NSString* urlPath = [NSString stringWithFormat:@"%@%@%@",LobbyAPIpath,urlAction,param];
    NSString* urlPath2 = [urlPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlPath2];
    
    //    NSLog(@"url:%@",urlPath2);
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data != nil) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            //            NSLog(@"json:%@",json);
            //            NSLog(@"status 2:%d",[json[@"status"] intValue]);
            
            UserSession* user = [UserSession sharedInstance];
            user.getPointSuccess = NO;
            
            if (json[@"status"] != nil && [json[@"status"] intValue] == 1 && json[@"result"] != nil) {
                
                NSString* resultDictStr = json[@"result"];
                NSData *resultDictJsonData = [resultDictStr dataUsingEncoding:NSUTF8StringEncoding];
                id resultDictJson = [NSJSONSerialization JSONObjectWithData:resultDictJsonData options:0 error:nil];
                
                if (resultDictJson != nil) {
                    user.point = [resultDictJson[@"sumCredit"] intValue];
                    user.getPointSuccess = YES;
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                if (self.leftMenuViewController != nil)
                    [(leftMenuViewController*)self.leftMenuViewController updateUserInfo];
            });
        }
    }];
    [dataTask resume];
}

- (void)callIncrementDailyPointAPI {
    
    UserSession* user = [UserSession sharedInstance];
    NSArray* tokenStr = [user.toKen componentsSeparatedByString:@"-"];
    NSString* tokenPassed = user.toKen;
    if ([tokenStr count] > 1) {
        tokenPassed = tokenStr[1];
    }
    
    NSString* param = [NSString stringWithFormat:@"?token=%@&channel=%@&otype=%@&ocredit=%@&sign=",tokenPassed,IOSUpdatePointChannel,@"2",DailyCheckInPoint];
    NSString* urlAction = @"credit/UpdateCredit";
    
    NSString* urlPath = [NSString stringWithFormat:@"%@%@%@",LobbyAPIpath,urlAction,param];
    NSString* urlPath2 = [urlPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlPath2];
    
    //    NSLog(@"url:%@",urlPath2);
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data != nil) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            //            NSLog(@"json:%@",json);
            //            NSLog(@"status 2:%d",[json[@"status"] intValue]);
            
            UserSession* user = [UserSession sharedInstance];
            user.getPointSuccess = NO;

            if (json[@"result"] != nil) {
                
                NSString* resultDictStr = json[@"result"];
                NSData *resultDictJsonData = [resultDictStr dataUsingEncoding:NSUTF8StringEncoding];
                id resultDictJson = [NSJSONSerialization JSONObjectWithData:resultDictJsonData options:0 error:nil];
                
                if (resultDictJson != nil && resultDictJson[@"sumCredit"] != nil) {
                    user.point = [resultDictJson[@"sumCredit"] intValue];
                    user.getPointSuccess = YES;
                }
            }

            BOOL updateAlert = NO;
            if (json[@"description"] != nil && [json[@"description"] isEqualToString:@"Successfully"] ) {
                
                updateAlert = YES;
                /*
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                [prefs setObject:[NSDate date] forKey:@"lastCheckInPointDate"];
                [prefs synchronize];
                 */
            }
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                if (self.leftMenuViewController != nil)
                    [(leftMenuViewController*)self.leftMenuViewController updateUserInfo];
                if (updateAlert) {
                    [self alertCheckIn];
                }
            });
        }
    }];
    [dataTask resume];
}

- (void)alertCheckIn {
    // alert increment point
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Daily Check In Point"
                                                                             message:[NSString stringWithFormat:@"You get %@ points!",DailyCheckInPoint]
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction* action){}];
    [alertController addAction:ok];
    [self.contentViewController presentViewController:alertController animated:YES completion:nil];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    // get user credits
    UserSession* user = [UserSession sharedInstance];
    if (user.userId != nil) {
        [self checkPointInfo];
    }
    
//    self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentViewNavController"];
    self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TrackerViewNavController"];
    self.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"leftMenuViewController"];
    
    AppDelegate* appDelegate = LobbyAppDelegate;
//    appDelegate.contentViewController = (contentViewController*)self.contentViewController;
    appDelegate.trackerViewController = (TrackerViewController*)self.contentViewController;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -
#pragma mark RESideMenu Delegate

- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"willShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"didShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"willHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"didHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

@end
