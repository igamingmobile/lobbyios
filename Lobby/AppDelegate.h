//
//  AppDelegate.h
//  Lobby
//
//  Created by Ivan Wan on 20/1/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import "AccountViewController.h"
#import "InviteViewController.h"
#import "TermsViewController.h"
#import "ContentTableViewController.h"
#import "NewsViewController.h"
#import "TrackerViewController.h"
#import "QRBeginViewController.h"
#import "DEMORootViewController.h"

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *productionVer;

@property (strong, nonatomic) AccountViewController *accountViewController;
@property (strong, nonatomic) InviteViewController *inviteViewController;
@property (strong, nonatomic) TermsViewController *termsViewController;
@property (strong, nonatomic) ContentTableViewController *contentTableViewController;
@property (strong, nonatomic) NewsViewController *newsViewController;
@property (strong, nonatomic) TrackerViewController *trackerViewController;
@property (strong, nonatomic) QRBeginViewController *qrBeginViewController;
@property (strong, nonatomic) DEMORootViewController *demoRootViewController;

@end

