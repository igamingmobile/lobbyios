//
//  TrackerDetaillViewController.m
//  Lobby
//
//  Created by Ivan Wan on 14/2/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import "TrackerDetaillViewController.h"

@interface TrackerDetaillViewController ()

@end

@implementation TrackerDetaillViewController
{
    UIDatePicker *datePicker;

    NSDate* iniDateValue;
    NSString* iniDate;
    NSInteger iniWinLose;
    NSString* iniAmount;
    NSString* iniName;
    NSString* iniLocation;
    NSString* iniNote;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Slot Record";
    
//    self.delBtn.layer.cornerRadius = 4;
//    self.delBtn.layer.masksToBounds = YES;
//    self.delBtn.layer.borderColor = [UIColor whiteColor].CGColor;
//    self.delBtn.layer.borderWidth = 1.0;
    
    self.saveBtn.layer.cornerRadius = 4;
    self.saveBtn.layer.masksToBounds = YES;
    self.saveBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    self.saveBtn.layer.borderWidth = 1.0;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    
    // this is a new record
    if (self.idStr == nil || [self.idStr length] < 1) {
//        [self.delBtn setHidden:YES];
//        [self.saveBtn setHidden:NO];
        
        NSString *dateString = [dateFormat stringFromDate:[NSDate date]];
        self.dateTextField.text = [NSString stringWithFormat:@"%@",dateString];
    }
    else {
//        [self.delBtn setHidden:NO];
//        [self.saveBtn setHidden:YES];
        
        UIBarButtonItem *infoBtn = [[UIBarButtonItem alloc]
                                    initWithTitle:[NSString stringWithFormat:@"%@", [NSString fontAwesomeIconStringForEnum:FATrashO]]
                                    style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(deleteRecord)];
        [infoBtn setTitleTextAttributes:@{  NSFontAttributeName: [UIFont fontWithName:kFontAwesomeFamilyName size:18.0f] ,
                                            NSForegroundColorAttributeName: [UIColor whiteColor]
                                            } forState:UIControlStateNormal];
        self.navigationItem.rightBarButtonItem = infoBtn;
    }
    
    UIBarButtonItem *btn=[[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem=btn;
    
    datePicker = [[UIDatePicker alloc]init];
    [datePicker setDate:[NSDate date]];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    [self.dateTextField setInputView:datePicker];
    
    //NSLog(@"id:%@",_idStr);
    
    // this is an edit action
    if (_idStr != NULL) {
        
        NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        NSString* filePath = [documentsPath stringByAppendingPathComponent:slotRecordFileName];
        
        NSDictionary *allD = [NSDictionary dictionaryWithContentsOfFile:filePath];
        NSDictionary *d = [allD objectForKey:_idStr];
        
        //NSLog(@"this d:%@",d);
        
        NSDate *eventDate = d[@"date"];
        NSString *dateString = [dateFormat stringFromDate:eventDate];
        self.dateTextField.text = [NSString stringWithFormat:@"%@",dateString];
        
        NSString* amount = d[@"amount"];
        if ([[amount substringToIndex:1] isEqualToString:@"-"]) {
            [self.winLoseSegmentView setSelectedSegmentIndex:1];
            [self.amountTextField setText:[amount substringFromIndex:1]];
        }
        else {
            [self.winLoseSegmentView setSelectedSegmentIndex:0];
            [self.amountTextField setText:amount];
        }
        
        [self.locTextField setText:d[@"location"]];
        [self.nameTextField setText:d[@"name"]];
        [self.noteTextView setText:d[@"note"]];
        
        iniDateValue = eventDate;
        iniDate = self.dateTextField.text;
        iniWinLose = self.winLoseSegmentView.selectedSegmentIndex;
        iniAmount = self.amountTextField.text;
        iniName = self.nameTextField.text;
        iniLocation = self.locTextField.text;
        iniNote = self.noteTextView.text;
    }
}

-(void)dateTextField:(id)sender
{
    datePicker = (UIDatePicker*)self.dateTextField.inputView;
    [datePicker setMaximumDate:[NSDate date]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *eventDate = datePicker.date;
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    
    NSString *dateString = [dateFormat stringFromDate:eventDate];
    self.dateTextField.text = [NSString stringWithFormat:@"%@",dateString];
}

- (IBAction)deleteRecord {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirm to delete record?" message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* delete = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [self confirmDelete];
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alertController addAction:delete];
    [alertController addAction:cancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)confirmDelete {
    NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString* filePath = [documentsPath stringByAppendingPathComponent:slotRecordFileName];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    
    NSMutableDictionary* d = [NSMutableDictionary new];
    
    // load existed data
    if (fileExists)
        d = [[NSDictionary dictionaryWithContentsOfFile:filePath] mutableCopy];
    
    [d removeObjectForKey:_idStr];
    
    BOOL success = [d writeToFile:filePath atomically:YES];
    NSAssert(success, @"writeToFile failed");
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)backAction
{
    if ([self.amountTextField.text length] == 0) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString* filePath = [documentsPath stringByAppendingPathComponent:slotRecordFileName];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    
    NSMutableDictionary* d = [NSMutableDictionary new];
    
    // load existed data
    if (fileExists) {
        d = [[NSDictionary dictionaryWithContentsOfFile:filePath] mutableCopy];
    }
    
    NSString* amount = self.amountTextField.text;
    if (self.winLoseSegmentView.selectedSegmentIndex == 1) {
        amount = [NSString stringWithFormat:@"-%@",amount];
    }

    NSString* thisID;
    NSDate* recordDate;
    
    // create new record
    if (_idStr == NULL) {
        // gen unique id
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        NSDate *date = [NSDate date];
        thisID = [dateFormatter stringFromDate:date];
        
        recordDate = datePicker.date;
    }
    // edit record
    else {
        thisID = _idStr;
        if ([iniDate isEqualToString:self.dateTextField.text]) {
            recordDate = iniDateValue;
        }
        else {
            recordDate = datePicker.date;
        }
    }
    
    NSDictionary* newD = @{@"id":thisID,
                           @"date":recordDate,
                           @"amount" : amount,
                           @"name" : self.nameTextField.text,
                           @"location" : self.locTextField.text,
                           @"note" : self.noteTextView.text,
                           };
    [d setValue:newD forKey:thisID];
    
    BOOL success = [d writeToFile:filePath atomically:YES];
    NSAssert(success, @"writeToFile failed");
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

@end
