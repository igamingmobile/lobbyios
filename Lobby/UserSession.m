//
//  UserSession.m
//  Lobby
//
//  Created by Ivan Wan on 1/2/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import "UserSession.h"

@implementation UserSession

@synthesize userId;
@synthesize toKen;
@synthesize nickname;
@synthesize email;
@synthesize phone;
@synthesize customId;
@synthesize sex;

@synthesize digits_authToken;
@synthesize digits_userID;
@synthesize digits_authTokenSecret;
@synthesize digits_phoneNumber;

@synthesize fb_userID;

@synthesize point;
@synthesize getPointSuccess;

#pragma mark sharedInstance
static UserSession* objSelf;
+ (UserSession*)sharedInstance {
    if (!objSelf) {
        static dispatch_once_t onceTokenControllerSession;
        dispatch_once(&onceTokenControllerSession, ^{
            objSelf = [[UserSession alloc] init];
        });
    }
    return objSelf;
}


@end
