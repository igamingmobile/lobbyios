//
//  UserSession.h
//  Lobby
//
//  Created by Ivan Wan on 1/2/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserSession : NSObject

@property (strong, nonatomic) NSString* userId;
@property (strong, nonatomic) NSString* toKen;
@property (strong, nonatomic) NSString* nickname;
@property (strong, nonatomic) NSString* email;
@property (strong, nonatomic) NSString* phone;
@property (strong, nonatomic) NSString* customId;
@property (strong, nonatomic) NSString* sex;

@property (strong, nonatomic) NSString* digits_authToken;
@property (strong, nonatomic) NSString* digits_userID;
@property (strong, nonatomic) NSString* digits_authTokenSecret;
@property (strong, nonatomic) NSString* digits_phoneNumber;

@property (strong, nonatomic) NSString* fb_userID;

@property (assign) int point;
@property (assign) BOOL getPointSuccess;

#pragma mark sharedInstance
+ (UserSession*)sharedInstance;

@end
