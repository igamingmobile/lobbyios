//
//  leftMenuViewController.h
//  Lobby
//
//  Created by Ivan Wan on 23/1/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"

#import "AccountViewController.h"
#import "InviteViewController.h"
#import "TermsViewController.h"
#import "contentViewController.h"

@interface leftMenuViewController : PortraitViewController

@property (strong, nonatomic) IBOutlet UIButton *accountSetupBkgrdBtn;
@property (strong, nonatomic) IBOutlet UIButton *accountSetupBtn;
@property (strong, nonatomic) IBOutlet UIImageView *profileIV;

@property (strong, nonatomic) IBOutlet UILabel *nameLbl;
@property (strong, nonatomic) IBOutlet UILabel *ptLbl;

@property (strong, nonatomic) IBOutlet UIButton *btnTracker;
@property (strong, nonatomic) IBOutlet UIButton *btnGames;
@property (strong, nonatomic) IBOutlet UIButton *btnInvite;
@property (strong, nonatomic) IBOutlet UIButton *btnNews;
@property (strong, nonatomic) IBOutlet UIButton *btnTerms;

@property (strong, nonatomic) IBOutlet UIButton *btnQRTest;
@property (strong, nonatomic) IBOutlet UIButton *btnCodeGen;

- (IBAction)btnTrackerAtn:(id)sender;
- (IBAction)btnAccountAtn:(id)sender;
- (IBAction)btnGameAtn:(id)sender;
- (IBAction)btnInviteAtn:(id)sender;
- (IBAction)btnNewsAtn:(id)sender;
- (IBAction)btnTermsAtn:(id)sender;
- (IBAction)btnQRAtn:(id)sender;
- (IBAction)btnCodeAtn:(id)sender;

- (void)updateUserInfo;

@end
