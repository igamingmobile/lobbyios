//
//  TrackerDetaillViewController.h
//  Lobby
//
//  Created by Ivan Wan on 14/2/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrackerDetaillViewController : UIViewController <UITextViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic)  NSString *idStr;

@property (weak, nonatomic) IBOutlet UISegmentedControl *winLoseSegmentView;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *locTextField;
@property (strong, nonatomic) IBOutlet UITextField *dateTextField;
@property (strong, nonatomic) IBOutlet UITextField *amountTextField;

@property (strong, nonatomic) IBOutlet UITextView *noteTextView;

//@property (strong, nonatomic) IBOutlet UIButton *delBtn;
@property (strong, nonatomic) IBOutlet UIButton *saveBtn;

@end
