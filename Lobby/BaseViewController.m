//
//  ViewController.m
//  Lobby
//
//  Created by Ivan Wan on 20/1/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    
    //    [self performSegueWithIdentifier:@"login_segue" sender:self];
    //    return;

    [self tryLogin];
}

- (void)tryLogin {

    
    NSString* token = @"";
    NSString* phone = @"";
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//    NSString* digits_authToken = [prefs stringForKey:@"digits_authToken"];
    
    if ([prefs stringForKey:@"phone"] != nil) {
        phone = [prefs stringForKey:@"phone"];
    }
    if ([prefs stringForKey:@"toKen"] != nil) {
        token = [prefs stringForKey:@"toKen"];
    }

    if (!token || [token length] == 0) {
        [self performSegueWithIdentifier:@"login_segue" sender:self];
    }
    else {
        
        // try login with token
        NSString* time = (NSString*)[NSDate date];
        NSString* sign = @"";
        
        NSString* param = [NSString stringWithFormat:@"phone=%@&time=%@&token=%@&sign=%@",phone,time,token,sign];
        NSString* param2 = [param stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        param2 = [param2 stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
        
        NSString* urlAction = @"user/mobile/login";
        NSString* urlPath = [NSString stringWithFormat:@"%@%@",TeamWorkAPIpath_noRest,urlAction];
        
//        NSLog(@"base | path :%@",urlPath);
//        NSLog(@"base | param:%@",param2);
        
        NSString* urlPath2 = [urlPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:urlPath2];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        request.HTTPMethod = @"POST";
        [request setHTTPBody:[param2 dataUsingEncoding:NSUTF8StringEncoding]];
        request.timeoutInterval = 5;
        
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
        
        NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:request
                                                           completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                               //NSLog(@"Response:%@ %@\n", response, error);
                                                               if(error != nil)
                                                               {
                                                                   //NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                                                                   //NSLog(@"Data = %@",text);

                                                                   [self performSegueWithIdentifier:@"login_segue" sender:self];
                                                               }
                                                               if (data != nil) {
                                                                   NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                   NSString* dictStr = json[@"result"];
                                                                   
                                                                   NSData *data2 = [dictStr dataUsingEncoding:NSUTF8StringEncoding];
                                                                   id resultDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                   if (data2) {
                                                                       resultDict = [NSJSONSerialization JSONObjectWithData:data2 options:0 error:nil];
                                                                   }
                                                                   else {
                                                                       UIAlertView *alert;
                                                                       alert = [[UIAlertView alloc]
                                                                                initWithTitle:@"There is a problem with the server. Please try login again later."
                                                                                message:nil
                                                                                delegate:self
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                                                       [alert show];
                                                                       
                                                                       [self performSegueWithIdentifier:@"menu_segue" sender:self];
                                                                   }
                                                                   
                                                                   // has user data
                                                                   if (resultDict) {
                                                                       NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                                                       [prefs setObject:resultDict[@"userId"] forKey:@"userId"];
                                                                       [prefs setObject:resultDict[@"phone"] forKey:@"phone"];
                                                                       [prefs setObject:resultDict[@"nickname"] forKey:@"nickname"];
                                                                       [prefs setObject:resultDict[@"email"] forKey:@"email"];
                                                                       [prefs setObject:resultDict[@"toKen"] forKey:@"toKen"];
                                                                       [prefs setObject:resultDict[@"customId"] forKey:@"customId"];
                                                                       [prefs setObject:resultDict[@"sex"] forKey:@"sex"];
                                                                       [prefs synchronize];
                                                                       
                                                                       UserSession* user = [UserSession sharedInstance];
                                                                       user.userId = resultDict[@"userId"];
                                                                       user.phone = resultDict[@"phone"];
                                                                       user.nickname = resultDict[@"nickname"];
                                                                       user.email = resultDict[@"email"];
                                                                       user.toKen = resultDict[@"toKen"];
                                                                       user.customId = resultDict[@"customId"];
                                                                       user.sex = resultDict[@"sex"];
                                                                       
                                                                       [self performSegueWithIdentifier:@"menu_segue" sender:self];
                                                                   }
                                                                   else {
                                                                       [self performSegueWithIdentifier:@"login_segue" sender:self];
                                                                   }
                                                               }
                                                           }];
        [dataTask resume];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)unwindToBaseViewController:(UIStoryboardSegue *)unwindSegue
{
    AppDelegate* appDelegate = LobbyAppDelegate;
    appDelegate.accountViewController = nil;

    [NSTimer scheduledTimerWithTimeInterval:0.5
                                                  target:self
                                                selector:@selector(tryLogin)
                                                userInfo:nil
                                                 repeats:NO];
}

@end
