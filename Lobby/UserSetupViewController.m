//
//  UserSetupViewController.m
//  Lobby
//
//  Created by Ivan Wan on 25/1/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import "UserSetupViewController.h"

@interface UserSetupViewController ()

@property (nonatomic, weak) UITextField *activeTextField;

@end

@implementation UserSetupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.title = @"Setup";
    
    // background gradient
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,ScreenWidth,ScreenHeight)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = view.bounds;
    gradient.startPoint = CGPointZero;
    gradient.endPoint = CGPointMake(1, 1);
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[[UIColor colorWithRed:218.0/255.0 green:8.0/255.0 blue:42/255.0 alpha:1.0] CGColor],
                       (id)[UIColorFromRGB(0xC643FC) CGColor], nil];
    [view.layer addSublayer:gradient];
    [self.view addSubview:view];
    [self.view sendSubviewToBack:view];
    
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Back"
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(backBtnClicked)];
    self.navigationItem.leftBarButtonItem = backButton;

    UIBarButtonItem *nextButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Done"
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(doneBtnClicked)];
    self.navigationItem.rightBarButtonItem = nextButton;
    
    [self.nameTextField becomeFirstResponder];
}

- (void)backBtnClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)doneBtnClicked {
    
    // gen unique id
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    NSDate *date = [NSDate date];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    int ran = arc4random_uniform(100000);
    NSString* uniqueID = [NSString stringWithFormat:@"%@%@",dateString,[NSString stringWithFormat:@"%05d", ran]];
    //NSLog(@"id:%@",uniqueID);
    
    NSString* token = @"";
    NSString* customID = @"";
    NSString* phone = @"";
    
    Digits* digits = [Digits sharedInstance];
    
    if (digits.session.authToken != nil) {
        token = digits.session.authToken;
        customID = digits.session.userID;
        phone = digits.session.phoneNumber;
    }
    else if ([FBSDKAccessToken currentAccessToken]) {
        token = [FBSDKAccessToken currentAccessToken].userID;
        customID = [FBSDKAccessToken currentAccessToken].userID;
    }
    
    // check if phone's first character is + or other non-digit
//    NSCharacterSet *fitstCharSet = [NSCharacterSet characterSetWithCharactersInString:phone];
//    if (![[NSCharacterSet decimalDigitCharacterSet] isSupersetOfSet:fitstCharSet])
//        phone = [phone substringFromIndex:1];
    
    NSString* nick = self.nameTextField.text;
    NSString* userID = uniqueID;
    NSString* email = @"";
    NSString* time = (NSString*)[NSDate date];
    NSString* sign = @"";
    
    NSString* param = [NSString stringWithFormat:@"phone=%@&nick=%@&userId=%@&email=%@&customId=%@&time=%@&token=%@&sign=%@",phone,nick,userID,email,customID,time,token,sign];
    [param stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    NSString* param2 = [param stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    param2 = [param2 stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    
    NSString* urlAction = @"user/mobile/register";
    NSString* urlPath = [NSString stringWithFormat:@"%@%@",TeamWorkAPIpath_noRest,urlAction];
    
    //NSLog(@"path:%@",urlPath);
    
    NSString* urlPath2 = [urlPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlPath2];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];

    request.HTTPMethod = @"POST";
    [request setHTTPBody:[param2 dataUsingEncoding:NSUTF8StringEncoding]];
    request.timeoutInterval = 5;
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:request
                                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                           //NSLog(@"Response:%@ %@\n", response, error);
                                                           if(error == nil || data == nil)
                                                           {
                                                               //NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                                                               //NSLog(@"Data = %@",text);

                                                               NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                               NSString* dictStr = json[@"result"];
                                                               
                                                               NSData *data = [dictStr dataUsingEncoding:NSUTF8StringEncoding];
                                                               id resultDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                               
                                                               // has user data
                                                               if (resultDict) {
                                                                   NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                                                   [prefs setObject:resultDict[@"userId"] forKey:@"userId"];
                                                                   [prefs setObject:resultDict[@"toKen"] forKey:@"toKen"];
                                                                   [prefs setObject:resultDict[@"nickname"] forKey:@"nickname"];
                                                                   [prefs setObject:resultDict[@"email"] forKey:@"email"];
                                                                   [prefs setObject:resultDict[@"phone"] forKey:@"phone"];
                                                                   [prefs setObject:resultDict[@"customId"] forKey:@"customId"];
                                                                   [prefs setObject:resultDict[@"sex"] forKey:@"sex"];
                                                                   [prefs synchronize];
                                                                   
                                                                   UserSession* user = [UserSession sharedInstance];
                                                                   user.userId = resultDict[@"userId"];
                                                                   user.phone = resultDict[@"phone"];
                                                                   user.nickname = resultDict[@"nickname"];
                                                                   user.email = resultDict[@"email"];
                                                                   user.toKen = resultDict[@"toKen"];
                                                                   user.customId = resultDict[@"customId"];
                                                                   user.sex = resultDict[@"sex"];
                                                                   
                                                                   // get user credits
                                                               }
                                                               
                                                              [self performSegueWithIdentifier:@"setup_menu_segue" sender:self];
                                                           }
                                                           else {
                                                               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Account can't be created"
                                                                                                               message:@"Please try to register later."
                                                                                                              delegate:nil
                                                                                                     cancelButtonTitle:@"OK"
                                                                                                     otherButtonTitles:nil];
                                                               [alert show];
                                                           }
                                                       }];
    [dataTask resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark TextField methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString* warningStr = nil;
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    if ((textField.tag == 0) && (newLength > 30)) {
        warningStr = [NSString stringWithFormat:NSLocalizedString(@"Name exceeds\n%d character limit", @""),30];
    }
    
    if (warningStr != nil) {
        // error case
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:warningStr
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                              otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.activeTextField = textField;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField.tag == 0){
        
        if ([textField.text length] == 0) {
            NSString* warningStr = @"Please enter a nick name.";
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:warningStr
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                                  otherButtonTitles:nil];
            [alert show];
            return NO;
            
        }
        [textField resignFirstResponder];
        
        [self doneBtnClicked];
    }
    return YES;
}

@end
